describe('Edit Post', () => {
    it('Editing a post to see if it works', () => {
      cy.visit('http://localhost:8000/login');
      cy.get('input[name="username"]').type('test@email.fr')
      cy.get('input[name="password"]').type('Azerty12345')
      cy.get('button.primary-trigger').click()

      cy.visit('http://localhost:8000/posts/add');


      cy.get('select[name="topic"]').select(1)

      cy.get('textarea[name="text"]').type('test')


      cy.get('[data-cy="goto-add-post"]').click()
      cy.get('[data-cy="goto-logout"]').click()
      expect(true).to.equal(true);
    })  
  });
  