describe('Login test', () => {
  it('Testing if user can log in with no problem', () => {
    cy.visit('http://localhost:8000/login');
    cy.get('input[name="username"]').type('test@email.fr');
    cy.get('input[name="password"]').type('Azerty12345');
    cy.get('[data-cy="submit"]').click();
    cy.get('.errorlist').should('not.exist');
    cy.get('[data-cy="goto-logout"]').click();
  });
  it('Testing if user can log in with problem (Invalid information (email))', () => {
    cy.visit('http://localhost:8000/login');
    cy.get('input[name="username"]').type('fake@fake.fake');
    cy.get('input[name="password"]').type('Azerty12345');
    cy.get('[data-cy="submit"]').click();
    cy.get('.errorlist').should(($p) => {
      expect($p).to.contain(
        'Saisissez un email et un mot de passe valides. Remarquez que chacun de ces champs est sensible à la casse (différenciation des majuscules/minuscules).'
      );
    });
  });
  it('Testing if user can log in with problem (Invalid information (password))', () => {
    cy.visit('http://localhost:8000/login');
    cy.get('input[name="username"]').type('test@email.fr');
    cy.get('input[name="password"]').type('Fake_fake');
    cy.get('[data-cy="submit"]').click();
    cy.get('.errorlist').should(($p) => {
      expect($p).to.contain(
        'Saisissez un email et un mot de passe valides. Remarquez que chacun de ces champs est sensible à la casse (différenciation des majuscules/minuscules).'
      );
    });
  });
});
