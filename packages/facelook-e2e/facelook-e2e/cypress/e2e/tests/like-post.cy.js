describe('Like a post', () => {
  it('Testing if user can log in and like a post', () => {
    cy.visit('http://localhost:8000/login');
    cy.get('input[name="username"]').type('test@email.fr');
    cy.get('input[name="password"]').type('Azerty12345');
    cy.get('[data-cy="submit"]').click();
    cy.get('.errorlist').should('not.exist');
    cy.visit('http://localhost:8000/topic/add');
    cy.get('input[name="title"]').type('test')
    cy.get('input[name="text"]').type('test')
    cy.get('button.primary-trigger').click()
    cy.visit('http://localhost:8000/posts/add');
    cy.get('[name="topic"]').select(1)
    cy.get('[name="text"]').type('test')
    cy.get('button.primary-trigger').click()
    cy.get('.errorlist').should('not.exist');
    cy.get('[data-cy="like-post"]').click()
    cy.get(".post-liked").should("exist");
    cy.get('.errorlist').should('not.exist');
    cy.get('[data-cy="goto-logout"]').click();
  });

  it('Testing if user can dislike a post', () => {
    cy.visit('http://localhost:8000/login');
    cy.get('input[name="username"]').type('test@email.fr');
    cy.get('input[name="password"]').type('Azerty12345');
    cy.get('[data-cy="submit"]').click();
    cy.get('.errorlist').should('not.exist');
    cy.visit('http://localhost:8000/topic/add');
    cy.get('input[name="title"]').type('test')
    cy.get('input[name="text"]').type('test')
    cy.get('button.primary-trigger').click()
    cy.visit('http://localhost:8000/posts/add');
    cy.get('[name="topic"]').select(1)
    cy.get('[name="text"]').type('test')
    cy.get('button.primary-trigger').click()
    cy.get('.errorlist').should('not.exist');
    cy.get('[data-cy="deslike-post"]').click()
    cy.get(".post-disliked").should("exist");
    cy.get('.errorlist').should('not.exist');
    cy.get('[data-cy="goto-logout"]').click();
  });
});
