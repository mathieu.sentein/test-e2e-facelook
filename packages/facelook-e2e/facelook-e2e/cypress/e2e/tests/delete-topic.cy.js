describe('Delete Topic', () => {
    it('Creation a new topic and deleting it', () => {
        cy.visit('http://localhost:8000/login');

        cy.get('input[name="username"]').type('test@email.fr')
        cy.get('input[name="password"]').type('Azerty12345')
        cy.get('button.primary-trigger').click()

        cy.visit('http://localhost:8000/topic/add');
        cy.get('input[name="title"]').type('test')
        cy.get('input[name="text"]').type('test')
        cy.get('button.primary-trigger').click()
        cy.get('[data-cy="goto-edit-topic"]').click()
        cy.get('[data-cy="delete-topic"]').click()
        cy.get('[data-cy="goto-logout"]').click()
        expect(true).to.equal(true);

    });
});
