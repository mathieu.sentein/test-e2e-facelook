describe('Post creation', () => {
    it('creating a new post', () => {
        cy.visit('http://localhost:8000/login');

        cy.get('input[name="username"]').type('test@email.fr')
        cy.get('input[name="password"]').type('Azerty12345')

        cy.get('button.primary-trigger').click()


        // Ajout d'un topic si besoin
        cy.visit('http://localhost:8000/topic/add');

        cy.get('input[name="title"]').type('test')
        cy.get('input[name="text"]').type('test')

        cy.get('button.primary-trigger').click()
        


        cy.visit('http://localhost:8000/posts/add');

        cy.get('[name="topic"]').select(0)
        cy.get('[name="text"]').type('test')

        cy.get('button.primary-trigger').click()


    });
});
