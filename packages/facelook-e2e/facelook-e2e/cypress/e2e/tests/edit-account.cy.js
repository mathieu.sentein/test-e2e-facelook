describe('Edit profile test', () => {
  it('Editing an account with no problem', () => {
    cy.visit('http://localhost:8000/login');
    cy.get('input[name="username"]').type('test@email.fr');
    cy.get('input[name="password"]').type('Azerty12345');
    cy.get('[data-cy="submit"]').click();

    cy.get('[data-cy="goto-profile"]').click();
    cy.get('[data-cy="goto-edit-profile"]').click();

    cy.get('[data-cy="first_name"]').type('2');
    cy.get('[data-cy="last_name"]').type('2');
    cy.get('[data-cy="email"]').type('s');
    cy.get('[data-cy="username"]').type('2');
    cy.get('[data-cy="password"]').type('Azerty12345');
    cy.get('[data-cy="bio"]').type('2');
    cy.get('[data-cy="bio"]').type('2');
    cy.get('[data-cy="submit"]').click();

    cy.get('.error').should('not.exist');
    cy.get('[data-cy="goto-logout"]').click();
  });





  it('Editing an account with problem (password not correct)', () => {
    cy.visit('http://localhost:8000/login');
    cy.get('input[name="username"]').type('test@email.frs');
    cy.get('input[name="password"]').type('Azerty12345');
    cy.get('[data-cy="submit"]').click();

    cy.get('[data-cy="goto-profile"]').click();
    cy.get('[data-cy="goto-edit-profile"]').click();

    cy.get('[data-cy="email"]').type('s');
    cy.get('[data-cy="password"]').type('Azerty123456789');
    cy.get('[data-cy="submit"]').click();

    cy.get('.error').should(($p) => {
      expect($p).to.contain('Informations incorrectes!');
    });
  });



  it('Editing an account with problem (email already exists)', () => {
    cy.visit('http://localhost:8000/signup')
    cy.get('input[name="first_name"]').type('test2')
    cy.get('input[name="last_name"]').type('test2')
    cy.get('input[name="username"]').type('test_test_test')
    cy.get('input[name="email"]').type('test_test@email.fr')
    cy.get('input[name="password1"]').type('Azerty12345')
    cy.get('input[name="password2"]').type('Azerty12345')
    cy.get('[data-cy="submit"]').click()


    cy.get('[data-cy="goto-profile"]').click();
    cy.get('[data-cy="goto-edit-profile"]').click()

    cy.get('[data-cy="email"]').clear();
    cy.get('[data-cy="email"]').type("test@email.frs"); //this email is used by tthe first user after the test
    cy.get('[data-cy="password"]').type('Azerty12345');
    cy.get('[data-cy="submit"]').click();
    cy.get('.error').should(($p) => {
      expect($p).to.contain('Email existe déjà!');
    });
  });



  it('Editing an account with problem (username already exists)', () => {
    cy.visit('http://localhost:8000/login');
    cy.get('input[name="username"]').type('test_test@email.fr');
    cy.get('input[name="password"]').type('Azerty12345');
    cy.get('[data-cy="submit"]').click();


    cy.get('[data-cy="goto-profile"]').click();
    cy.get('[data-cy="goto-edit-profile"]').click()

    cy.get('[data-cy="username"]').clear();
    cy.get('[data-cy="username"]').type("test_test2"); //this email is used by tthe first user
    cy.get('[data-cy="password"]').type('Azerty12345');
    cy.get('[data-cy="submit"]').click();
    cy.get('.error').should(($p) => {
      expect($p).to.contain('Pseudo exite déjà!');
    });
  });


});




describe('Reset user informations', () => {
  it('Does not do much!', () => {
    cy.visit('http://localhost:8000/login');

    cy.get('input[name="username"]').type('test@email.frs');
    cy.get('input[name="password"]').type('Azerty12345');
    cy.get('[data-cy="submit"]').click();

    cy.get('[data-cy="goto-profile"]').click();
    cy.get('[data-cy="goto-edit-profile"]').click();

    cy.get('[data-cy="first_name"]').clear();
    cy.get('[data-cy="first_name"]').type('test');

    cy.get('[data-cy="last_name"]').clear();
    cy.get('[data-cy="last_name"]').type('test');

    cy.get('[data-cy="email"]').clear();
    cy.get('[data-cy="email"]').type('test@email.fr');

    cy.get('[data-cy="username"]').clear();
    cy.get('[data-cy="username"]').type('test_test');

    cy.get('[data-cy="password"]').type('Azerty12345');

    cy.get('[data-cy="bio"]').clear();
    cy.get('[data-cy="bio"]').type('Pas encore de bio');

    cy.get('[data-cy="submit"]').click();
    expect(true).to.equal(true);
  });
});
