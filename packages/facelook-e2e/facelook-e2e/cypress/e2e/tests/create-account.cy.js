describe('Account creation', () => {
  it('Creating a new account of Facelook with no problem', () => {
    cy.visit('http://localhost:8000/signup');

    cy.get('input[name="first_name"]').type('test')
    cy.get('input[name="last_name"]').type('test')
    cy.get('input[name="username"]').type('test_test')
    cy.get('input[name="email"]').type('test@email.fr')
    cy.get('input[name="password1"]').type('Azerty12345')
    cy.get('input[name="password2"]').type('Azerty12345')
    cy.get('[data-cy="submit"]').click()
    cy.get('[data-cy="goto-logout"]').click()
  });
  it('Creating a new account of Facelook with problem (passwords do not match)', () => {
    cy.visit('http://localhost:8000/signup');
    cy.get('input[name="first_name"]').type('test')
    cy.get('input[name="last_name"]').type('test')
    cy.get('input[name="username"]').type('test_test')
    cy.get('input[name="email"]').type('test@email.fr')
    cy.get('input[name="password1"]').type('Azerty12345')
    cy.get('input[name="password2"]').type('Azerty123456')
    cy.get('[data-cy="submit"]').click()
    cy.get('.errorlist').should(($p) => {
      expect($p).to.contain('Les deux mots de passe ne correspondent pas.') 
    })
  });
  it('Creating a new account of Facelook with problem (email already exists)', () => {
    cy.visit('http://localhost:8000/signup');
    cy.get('input[name="first_name"]').type('test')
    cy.get('input[name="last_name"]').type('test')
    cy.get('input[name="username"]').type('test_test2')
    cy.get('input[name="email"]').type('test@email.fr')
    cy.get('input[name="password1"]').type('Azerty12345')
    cy.get('input[name="password2"]').type('Azerty12345')
    cy.get('[data-cy="submit"]').click()
    cy.get('.errorlist').should(($p) => {
      expect($p).to.contain('Un objet Utilisateur avec ce champ Email existe déjà.') 
    })
  });
  it('Creating a new account of Facelook with problem (username already exists)', () => {
    cy.visit('http://localhost:8000/signup');
    cy.get('input[name="first_name"]').type('test')
    cy.get('input[name="last_name"]').type('test')
    cy.get('input[name="username"]').type('test_test')
    cy.get('input[name="email"]').type('test2@email.fr')
    cy.get('input[name="password1"]').type('Azerty12345')
    cy.get('input[name="password2"]').type('Azerty12345')
    cy.get('[data-cy="submit"]').click()
    cy.get('.errorlist').should(($p) => {
      expect($p).to.contain('Un utilisateur avec ce nom existe déjà.') 
    })
  })
});
