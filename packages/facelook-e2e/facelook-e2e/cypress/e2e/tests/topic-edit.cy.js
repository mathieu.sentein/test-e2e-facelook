describe('Edit topic', () => {
    it('Editing a topic to see if it works', () => {
      cy.visit('http://localhost:8000/login');
      cy.get('input[name="username"]').type('test@email.fr')
      cy.get('input[name="password"]').type('Azerty12345')
      cy.get('button.primary-trigger').click()

      cy.visit('http://localhost:8000/topic/add');

      cy.get('input[name="title"]').type('test')
      cy.get('input[name="text"]').type('test')

      cy.get('button.primary-trigger').click()


      cy.get('[data-cy="goto-edit-topic"]').click()

      cy.get('input[name="title"]').clear()
      cy.get('input[name="title"]').type("new test")

      cy.get('input[name="text"]').clear()
      cy.get('input[name="text"]').type("new test")


      cy.get('[data-cy="submit"]').click()
      cy.get('[data-cy="goto-logout"]').click()
      expect(true).to.equal(true);
    })  
  });
  