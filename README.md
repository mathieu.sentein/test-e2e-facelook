Projet e2e sur facelook.

Le projet a été fait par :
Mathieu Sentein
GHIZLAN Moqim

Le projet de base est un petit clone de Facebook nous avons décidé de le nommer Facelook.
Voici le lien vers le projet de base au cas où :
https://gitlab.com/lp-1a/facelook


Pour bien utiliser les test, merci de suivre les étapse suivants:

RDV dans le dossier test-e2e-facelook
 Pour instailler les utils
 $ npm i
 ensuite vous lancez les commends de NX dans l'ordre,
 createEnv // ---> qui va vous créer un enverement de python
 activate // ---> qui va vous activer l'env
 install // ---> qui va vous installer les packages de python via pip
 makemigration // ---> qui va vous créer le bd local (ou pas)
 migrate // ---> qui va vous créer les tables dans le bd local
 serve  // ---> qui va vous lancer le serveur sur le port 8000

Ensuite vous lancer les test d'e2e.
